const express = require('express');
const bodyParser = require('body-parser');
const graphqlHttp = require('express-graphql');
const MongoConnect = require('./utils/db');
const graphQlSchema = require('./graphql/schema/index');
const graphQlResolvers = require('./graphql/resolvers/index');
const isAuth = require('./middleware/is-auth');
const port = 8000;
const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200);
  }
  next();
});

app.use(isAuth);

app.use(
  '/graphql',
  graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
  })
);

const main = async () => {
  try {
    const connection = await MongoConnect();
    if (connection) {
      console.log("Data Base connected !");
      app.listen(port, () => console.log(`Server Running on port ${port}`));
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};
main();
