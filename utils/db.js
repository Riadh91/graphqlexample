const mongoose = require("mongoose");

const MongoConnect = async () => {
  try {
    return await mongoose.connect(`mongodb://localhost/EventApp`, {
      useNewUrlParser: true
    });
  } catch (err) {
    console.log("MongoDb error is:", err);
    throw err;
  }
};

module.exports = MongoConnect;
